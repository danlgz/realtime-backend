import http from 'http'
import express from 'express'
import path from 'path'
import socketio from 'socket.io'

const port = process.env.PORT || 3002
const app = express()
const server = http.createServer(app)
const io = socketio(server)
const socketChat = io.of('/socket-chat')
let usersonline = [];

app.use(express.static(path.join(__dirname, 'public')))
// Socket.io || Web Socket
socketChat.on('connection', (socket) => {
    console.log(`++ Chat establecido, ID: ${socket.id}`)

    socket.on('disconnect', (data) => {
        console.log(`-- Chat desconectado, ID: ${socket.id}`)
        let index = usersonline.findIndex(user => user.id == socket.id)
        if (index != -1) {
            console.log(`usuario ${usersonline[index].username} deslogeado`)
            usersonline.splice(index, 1)
            socketChat.emit('usersonline', {
                users: usersonline
            })
        }
    })

    socket.on('logout', (userLogout) => {
        let index = usersonline.findIndex(userTemp => userTemp.id == userLogout.id)
        if (index != -1) {
            console.log(`usuario ${usersonline[index].username} deslogeado`)
            usersonline.splice(index, 1)
        }
        socketChat.emit('usersonline', {
            users: usersonline
        })
    })

    socket.on('login', (username) => {
        console.log(`usuario ${username} logeado`)
        let index = usersonline.findIndex(user => user == username)
        if (index == -1) {
            usersonline.push({
                username: username,
                id: socket.id
            })
        }
        socket.emit('login-response', {
            username: username,
            id: socket.id
        })
        console.log(JSON.stringify(usersonline))
        socketChat.emit('usersonline', {
            users: usersonline
        })
    })

    socket.on('new-message', (message) => {
        socketChat.emit(message.to, message)
    })
})

server.listen(port, () => {
    console.log(`API-Realtime en puerto ${port}`)
})